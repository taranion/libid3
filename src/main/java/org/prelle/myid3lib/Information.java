package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public class Information {

	private InfoType type;
	private Object data;
	
	//-----------------------------------------------------------------
	/**
	 */
	public Information(InfoType type, Object data) {
		this.type = type;
		this.data = data;
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return data.toString();
	}

	//-----------------------------------------------------------------
	/**
	 * @return the type
	 */
	public InfoType getType() {
		return type;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

}
