/**
 * 
 */
package org.prelle.myid3lib;


/**
 * @author prelle
 *
 */
public abstract class AbstractID3Header {
	
	protected ID3Version version;

	//-----------------------------------------------------------------
	/**
	 */
	public AbstractID3Header(ID3Version version) {
		this.version = version;
	}

	//-----------------------------------------------------------------
	public ID3Version getVersion() {
		return version;
	}

}
