/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public enum MPEGLayer {

	RESERVED,
	LAYER_3,
	LAYER_2,
	LAYER_1,
}
