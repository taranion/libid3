/**
 *
 */
package org.prelle.myid3lib;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.BitSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.v1.Version1Reader;
import org.prelle.myid3lib.v2.BigEndianBitSet;
import org.prelle.myid3lib.v2.Version2Reader;

/**
 * @author prelle
 *
 */
public class TagReader {

	private final static Logger logger = LogManager.getLogger("id3");

	private static BitSet SYNCHEADER = new BitSet(32);

	//-----------------------------------------------------------------
	static {
		SYNCHEADER.set(0, 11);
	}

	//-----------------------------------------------------------------
	public static TaggedFile read(Path file) throws IOException, TagException {
		TaggedFile ret = new TaggedFile(file);

		ret.setV1(Version1Reader.read(file));
		ret.setV2(Version2Reader.read(file));

		long mpgStart = 0;
		if (ret.getV2()!=null) {
			mpgStart = ret.getV2().getTagSize();
		}

		SeekableByteChannel buffer = Files.newByteChannel(file);
		buffer.position(mpgStart);
		checkMPGHeader(buffer, ret);

		// Guess
		long mpgsize = Files.size(file) - mpgStart;
		if (ret.getBitrate()>0) {
			long divider = (ret.getBitrate()/8) * 1000;
			ret.setDuration((int)(mpgsize/divider));
			logger.debug("Duration is "+ret.getDuration());
		}


		return ret;
	}

//	//-----------------------------------------------------------------
//	private static int asInt(byte b) {
//		if (b<0) {
//			return 256+b;
//		}
//		return b;
//	}

	//-----------------------------------------------------------------
	/**
	 * Check if there is a MPEG header at the current file position
	 */
	private static void checkMPGHeader(SeekableByteChannel file, TaggedFile tagged) throws IOException {
		byte[] mpgFrame = new byte[4];
		ByteBuffer buffer = ByteBuffer.wrap(mpgFrame);
		file.read(buffer);

		logger.debug("CheckMPG = "+HexDumpMaker.makeHexDump(mpgFrame));
		BigEndianBitSet syncHeader = new BigEndianBitSet(mpgFrame);
		if (!syncHeader.matches(SYNCHEADER, 0, 10)) {
			logger.debug("No MPEG header found");
			return;
		}
		logger.debug("Header bits = "+syncHeader);

		MPEGVersion version = MPEGVersion.values()[syncHeader.getAsInt(11, 12)];
		MPEGLayer   layer = MPEGLayer.values()[syncHeader.getAsInt(13, 14)];
		int bitrate = Bitrate.getBitrateKBit(version, layer, syncHeader.getAsInt(16, 19));
		int sample = syncHeader.getAsInt(20, 21);

		logger.debug("id="+version+"  layer="+layer+"  bitrate="+bitrate+"kbit/s   samplerate="+sample);

		tagged.setBitrate(bitrate);
		tagged.setMpegLayer(layer);
		tagged.setMpegVersion(version);
	}

}
