/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public enum ID3Version {

	V0("V1.0"),
	V1("V1.1"),
	V2("V2.2"),
	V3("V2.3"),
	V4("V2.4"),
	;
	
	private String name;
	
	ID3Version(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
}
