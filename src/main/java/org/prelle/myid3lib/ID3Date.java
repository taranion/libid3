/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public class ID3Date {
	
	private int year;
	private int month = -1;
	private int day = -1;

	//-----------------------------------------------------------------
	public static ID3Date parse(String toParse) {
		if (toParse.length()<=4)
			return new ID3Date(Integer.parseInt(toParse));
		System.err.println("ID3Date.parse: Don't know how to parse ["+toParse+"]");
		return new ID3Date(Integer.parseInt(toParse.substring(0, Math.min(4, toParse.length()))));
	}

	//-----------------------------------------------------------------
	public ID3Date(int year) {
		this.year = year;
	}

	//-----------------------------------------------------------------
	public String toString() {
		if (month==-1)
			return year+"";
		if (day==-1)
			return year+"-"+month;
		return year+"-"+month+"-"+day;
			
	}

	//-----------------------------------------------------------------
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

}
