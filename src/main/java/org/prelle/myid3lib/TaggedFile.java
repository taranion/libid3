/**
 * 
 */
package org.prelle.myid3lib;

import java.nio.file.Path;

import org.prelle.myid3lib.v1.Version1Header;
import org.prelle.myid3lib.v2.Version2Header;

/**
 * @author prelle
 *
 */
public class TaggedFile {
	
	private Path file;
	
	private Version1Header v1;
	private Version2Header v2;
	private MPEGVersion mpegVersion;
	private MPEGLayer   mpegLayer;
	private int         bitrate;
	/** Estimated duration in seconds */
	private int         duration;

	//-----------------------------------------------------------------
	/**
	 */
	TaggedFile(Path file) {
		this.file = file;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the v10
	 */
	public Version1Header getV1() {
		return v1;
	}

	//-----------------------------------------------------------------
	/**
	 * @param v1 the v10 to set
	 */
	public void setV1(Version1Header v1) {
		this.v1 = v1;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the v2
	 */
	public Version2Header getV2() {
		return v2;
	}

	//-----------------------------------------------------------------
	/**
	 * @param v2 the v2 to set
	 */
	public void setV2(Version2Header v2) {
		this.v2 = v2;
	}

	//-----------------------------------------------------------------
	public String getTitle() {
		if (v2!=null) {
			if (v2.has("TPE1")) return v2.getAsString("TPE1");
			if (v2.has("TPE2")) return v2.getAsString("TPE2");
			if (v2.has("TPE3")) return v2.getAsString("TPEe");
		}
		if (v1!=null)
			return v1.getArtist();
		
		return null;
	}

	//-----------------------------------------------------------------
	public String getArtist() {
		if (v2!=null) {
			if (v2.has("TPE1")) return v2.getAsString("TPE1");
			if (v2.has("TPE2")) return v2.getAsString("TPE2");
			if (v2.has("TPE3")) return v2.getAsString("TPEe");
		}
		if (v1!=null)
			return v1.getArtist();
		
		return null;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the mpegVersion
	 */
	public MPEGVersion getMpegVersion() {
		return mpegVersion;
	}

	//-----------------------------------------------------------------
	/**
	 * @param mpegVersion the mpegVersion to set
	 */
	public void setMpegVersion(MPEGVersion mpegVersion) {
		this.mpegVersion = mpegVersion;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the mpegLayer
	 */
	public MPEGLayer getMpegLayer() {
		return mpegLayer;
	}

	//-----------------------------------------------------------------
	/**
	 * @param mpegLayer the mpegLayer to set
	 */
	public void setMpegLayer(MPEGLayer mpegLayer) {
		this.mpegLayer = mpegLayer;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the bitrate
	 */
	public int getBitrate() {
		return bitrate;
	}

	//-----------------------------------------------------------------
	/**
	 * @param bitrate the bitrate to set
	 */
	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	//-----------------------------------------------------------------
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	//-------------------------------------------------------------------
	public Path getFile() {
		return file;
	}
}
