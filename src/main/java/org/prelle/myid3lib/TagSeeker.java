package org.prelle.myid3lib;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;

/**
 * @author prelle
 *
 */
public class TagSeeker {
	
	//-----------------------------------------------------------------
	public void searchTags(SeekableByteChannel file) {
		
	}

	//-----------------------------------------------------------------
	public ID3Version searchV1Tag(SeekableByteChannel file) throws IOException {
    	ID3Version detected = null;
    	
        // If there is a tag, it is 127 bytes long and we'll find the tag
        file.position(file.size() - 128);
        ByteBuffer buffer = ByteBuffer.allocate(3);

        // read the TAG value
        file.read(buffer);
        final String tag = new String(buffer.array(), 0, 3);
        if (!tag.equals("TAG"))
        	return null;
    	detected = ID3Version.V0;
        
        // Check for the empty byte before the TRACK
    	buffer.clear();
        file.position(file.size() - 3);
        file.read(buffer);
        if (buffer.array()[0] == 0) 
            detected = ID3Version.V1;

        file.position(file.size() - 125);
        return detected;
	}

	//-----------------------------------------------------------------
	public boolean searchExtendedV1Tag(SeekableByteChannel file) throws IOException {
        // Check for extended v1 tag
        file.position(file.size() -128 -227);
        ByteBuffer buffer = ByteBuffer.allocate(4);
        file.read(buffer);
        String tag2 = new String(buffer.array(), 0, 4);
        if (tag2.equals("TAG+")) {
        	System.err.println("Version1Reader.detectV1Version: Extended v1 tag found");
        	System.exit(0);
        	return true;
        }
        	
        return false;
	}

}
