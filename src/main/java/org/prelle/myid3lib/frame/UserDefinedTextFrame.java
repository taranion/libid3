/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class UserDefinedTextFrame extends ID3v2Frame {
	
	private String description;
	private String value;

	//-----------------------------------------------------------------
	public UserDefinedTextFrame(String frameID, String desc, String val) {
		super(frameID);
		this.description = desc;
		this.value = val;
	}

	//-----------------------------------------------------------------
	public String toString() {
		return identifier+": "+description+" = "+value;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
