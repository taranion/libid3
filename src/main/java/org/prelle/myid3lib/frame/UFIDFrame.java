/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class UFIDFrame extends ID3v2Frame {
	
	private String owner;
	private String uniqID;

	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 */
	public UFIDFrame(String frameID) {
		super(frameID);
	}

	//-----------------------------------------------------------------
	public String toString() {
		return "UFID:(NS="+owner+") = "+uniqID;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	//-----------------------------------------------------------------
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the identifier
	 */
	public String getUniqID() {
		return uniqID;
	}

	//-----------------------------------------------------------------
	/**
	 * @param identifier the identifier to set
	 */
	public void setUniqID(String identifier) {
		this.uniqID = identifier;
	}

}
