/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class COMMFrame extends ID3v2Frame {
	
	private String language;
	private String shortDescription;
	private String fullComment;

	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 */
	public COMMFrame(String frameID) {
		super(frameID);
	}

	//-----------------------------------------------------------------
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	//-----------------------------------------------------------------
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	//-----------------------------------------------------------------
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the fullComment
	 */
	public String getFullComment() {
		return fullComment;
	}

	//-----------------------------------------------------------------
	/**
	 * @param fullComment the fullComment to set
	 */
	public void setFullComment(String fullComment) {
		this.fullComment = fullComment;
	}

}
