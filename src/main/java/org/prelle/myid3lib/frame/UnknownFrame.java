/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class UnknownFrame extends ID3v2Frame {

	private byte[] data;
	
	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 */
	public UnknownFrame(String frameID, byte[] data) {
		super(frameID);
		this.data = data;
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return identifier+": Unknown "+data.length+" bytes";
	}
	
	//-----------------------------------------------------------------
	public byte[] getContent() {
		return data;
	}

}
