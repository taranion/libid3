/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class APICFrame extends ID3v2Frame {
	
	public enum PictureType {
		OTHER("Other"),
		ICON32PX("32x32 pixels 'file icon' (PNG only)"),
		ICON("Other file icon"),
		COVER_FRONT("Cover (front"),
		COVER_BACK("Cover (back"),
		LEAFLET_PAGE("Leaflet_Page"),
		MEDIA("Media (e.g. label side of CD)"),
        LEAD_ARTIST("Lead artist/lead performer/soloist"),
        ARTIST("Artist/performer"),
        CONDUCTOR("Conductor"),
        BAND("Band/Orchestra"),
        COMPOSER("Composer"),
        TEXTER("Lyricist/text writer"),
        RECORDING_LOCATION("Recording Location"),
        DURING_RECORDING("During recording"),
        DURING_PERFORMANCE("During performance"),
        SCREEN_CAPTURE("Movie/video screen capture"),
        A_BRIGHT_COLORED_FISH("A bright coloured fish"),
        ILLUSTRATION("Illustration"),
        BAND_LOGO("Band/artist logotype"),
        PUBLISHER_LOGO("Publisher/Studio logotype"),
        ;
		private String description;
		PictureType(String desc) {
			this.description = desc;
		}
		public String getDescription() { return description;}
	}
	
	private String mimeType;
	private PictureType pictureType;
	private String description;
	private byte[] imageData;

	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 */
	public APICFrame(String frameID) {
		super(frameID);
		// TODO Auto-generated constructor stub
	}

	//-----------------------------------------------------------------
	public String toString() {
		return description+" ("+pictureType+", "+mimeType+")";
	}

	//-----------------------------------------------------------------
	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	//-----------------------------------------------------------------
	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the pictureType
	 */
	public PictureType getPictureType() {
		return pictureType;
	}

	//-----------------------------------------------------------------
	/**
	 * @param pictureType the pictureType to set
	 */
	public void setPictureType(PictureType pictureType) {
		this.pictureType = pictureType;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-----------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the imageData
	 */
	public byte[] getImageData() {
		return imageData;
	}

	//-----------------------------------------------------------------
	/**
	 * @param imageData the imageData to set
	 */
	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

}
