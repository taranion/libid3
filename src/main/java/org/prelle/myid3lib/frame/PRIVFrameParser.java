package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.frame.TextFrameParser.Encoding;

/**
 * @author prelle
 *
 */
public class PRIVFrameParser implements FrameParser {

	private final static Logger logger = LogManager.getLogger("id3.frame");

	//-----------------------------------------------------------------
	public PRIVFrameParser() {
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.frame.FrameParser#parse(java.lang.String, java.nio.ByteBuffer, int)
	 */
	@Override
	public ID3v2Frame parse(String frameID, ByteBuffer buffer, int length) {
		int maxPos = buffer.position() + length;
		logger.debug("parse from "+buffer.position()+" to "+maxPos);

		String owner = TextFrameParser.readUntilNullByte(buffer, Encoding.ISO_8859_1, maxPos);
		logger.debug("owner = "+owner);
		byte[] data = new byte[buffer.remaining()];
		buffer.get(data);
		logger.debug("data = "+data.length+" bytes");

		PRIVFrame ret = new PRIVFrame(frameID);
		ret.setOwner(owner);
		ret.setData(data);
		return ret;
	}

}
