package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.frame.APICFrame.PictureType;
import org.prelle.myid3lib.frame.TextFrameParser.Encoding;

/**
 * @author prelle
 *
 */
public class APICFrameParser implements FrameParser {

	private final static Logger logger = LogManager.getLogger("id3.frame");

	//-----------------------------------------------------------------
	public APICFrameParser() {
		// TODO Auto-generated constructor stub
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.frame.FrameParser#parse(java.lang.String, java.nio.ByteBuffer, int)
	 */
	@Override
	public ID3v2Frame parse(String frameID, ByteBuffer buffer, int length) {
		int maxPos = buffer.position() + length;
		logger.debug("parse from "+buffer.position()+" to "+maxPos);

		Encoding encoding = Encoding.ISO_8859_1;
		try {
			encoding = Encoding.values()[buffer.get()];
		} catch (IllegalArgumentException e) {
		}
		logger.debug("APIC Desc encoding = "+encoding);
		String mimeType = TextFrameParser.readUntilNullByte(buffer, Encoding.ISO_8859_1, buffer.position()+127);

		logger.debug("mime-type = "+mimeType);
		byte type = buffer.get();
		logger.debug("type = "+type);
		String description = TextFrameParser.readUntilNullByte(buffer, encoding, buffer.position()+255);
		logger.debug("desc = "+description);

		byte[] imageData = new byte[maxPos-buffer.position()];
		buffer.get(imageData);
		logger.debug("image data = "+imageData.length+" bytes");

		APICFrame ret = new APICFrame(frameID);
		ret.setDescription(description);
		ret.setPictureType(PictureType.values()[type]);
		ret.setMimeType(mimeType);
		ret.setImageData(imageData);
		return ret;
	}

}
