/**
 * 
 */
package org.prelle.myid3lib.frame;


/**
 * @author prelle
 *
 */
public abstract class ID3v2Frame {

	protected String identifier;
	
	//-----------------------------------------------------------------
	/**
	 */
	public ID3v2Frame(String frameID) {
		this.identifier = frameID;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return "frame '"+identifier+"'";
	}

}
