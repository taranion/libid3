/**
 * 
 */
package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;

/**
 * @author prelle
 *
 */
public interface FrameParser {

	//-----------------------------------------------------------------
	public ID3v2Frame parse(String frameID, ByteBuffer buffer, int length);
	
}
