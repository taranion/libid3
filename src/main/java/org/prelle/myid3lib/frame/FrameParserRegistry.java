/**
 *
 */
package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.HexDumpMaker;

/**
 * @author prelle
 *
 */
public class FrameParserRegistry {

	private final static Logger logger = LogManager.getLogger("id3.frame");

	private static TextFrameParser text;
	private static Map<String, FrameParser> registered;

	//-----------------------------------------------------------------
	static {
		registered = new HashMap<String, FrameParser>();

		text = new TextFrameParser();;
		register("APIC", new APICFrameParser());
		register("COMM", new COMMFrameParser());
		register("PRIV", new PRIVFrameParser());
		register("TALB", text);
		register("TPE1", text);
		register("UFID", new UFIDFrameParser());
	}

	//-----------------------------------------------------------------
	public static void register(String frameID, FrameParser parser) {
		if (registered.containsKey(frameID)) {
			if (registered.get(frameID).getClass().equals(parser.getClass()))
				return;
			throw new IllegalStateException("Frame "+frameID+" already registered with parser "+registered.get(frameID).getClass());
		}

		registered.put(frameID, parser);
	}

	//-----------------------------------------------------------------
	public static FrameParser getParser(String frameID) {
		return registered.get(frameID);
	}

	//-----------------------------------------------------------------
	public static ID3v2Frame parse(String frameID, ByteBuffer buffer, int length) {
		if (length<1)
			throw new IllegalArgumentException("Invalid length - was "+length);
		if (logger.isTraceEnabled() && length<128) {
			int pos = buffer.position();
			byte[] foo = new byte[length];
			buffer.get(foo);
			logger.debug("parse from "+pos+" to "+buffer.position()+"\n"+HexDumpMaker.makeHexDump(foo));
			buffer.position(pos);
		}

//		int nextBufferIsAt = buffer.position() + length;
//		logger.debug("Next Frame is at "+nextBufferIsAt+" (current="+buffer.position()+", length="+length+")");

		FrameParser parser = registered.get(frameID);
		if (parser==null) {
			// Unknown frame type
			// For Text frame, use generic text frame
			if (frameID.startsWith("T")) {
				ID3v2Frame ret = text.parse(frameID, buffer, length);
				// Ensure correct next position
//				buffer.position(nextBufferIsAt);
				return ret;
			}
			byte[] foo = new byte[length];
			buffer.get(foo);

			if (length<128) {
				logger.warn("Unsupported frame: "+frameID+"\n"+HexDumpMaker.makeHexDump(foo));
			}
			// Ensure correct next position
//			buffer.position(nextBufferIsAt);
			return new UnknownFrame(frameID, foo);
		} else {
			ID3v2Frame ret = parser.parse(frameID, buffer, length);
			// Ensure correct next position
//			buffer.position(nextBufferIsAt);
			return ret;
		}
	}

}
