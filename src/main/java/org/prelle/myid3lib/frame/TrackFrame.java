/**
 * 
 */
package org.prelle.myid3lib.frame;

import java.util.StringTokenizer;

/**
 * @author prelle
 *
 */
public class TrackFrame extends TextFrame {
	
	private transient int track = -1;
	private transient int maxTracks = -1;

	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 * @param content
	 */
	public TrackFrame(String frameID, String content) {
		super(frameID, content);
		if (content.length()==0)
			return;
		StringTokenizer tok = new StringTokenizer(content, "/");
		track = Integer.parseInt(tok.nextToken());
		if (tok.hasMoreTokens())
			maxTracks = Integer.parseInt(tok.nextToken());
	}

	//-----------------------------------------------------------------
	/**
	 * @return the track
	 */
	public int getTrack() {
		return track;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the maxTracks
	 */
	public int getMaxTracks() {
		return maxTracks;
	}

}
