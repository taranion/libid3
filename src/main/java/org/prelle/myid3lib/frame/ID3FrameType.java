package org.prelle.myid3lib.frame;

public enum ID3FrameType {
	AENC("Audio encryption"),
	APIC("Picture"),
	COMM("Comments"),
	COMR("Commercial"),	
	PRIV("Private"), // Private (depends on owner)
	RVA2("Rel.Vol.Adj"),
	TALB("Album"),
	TCON("Genre"),
	TDRC("Recording Time"),
	TDOR("Rel.Time"), // Original release time
	TENC("Encoded by"),
	TEXT("TEXT"),
	TLEN("TLEN"),
	TPE1("Performer"), // Lead Performer / Soloist
	TPE2("Band"), // Band / Orchestra
	TPE3("Conductor"), // Conductor / Performer refinement
	TPOS("Part of Set"), // Part of Set
	TPUB("TPUB"),
	TRCK("TRCK"),
	TSIZ("Size"), // Deprecated in 2.4
	TSOA("Album Sort"), // Album Sort Order
	TSOP("Artist Sort"), // Artist Sort Order
	TSO2("AlbArt Sort"), // AlbumArtist Sort Order (iTunes)
	TSOT("Title Sort"),
	TIT2("TIT2"),
	TYER("Year"), // Year (replaced by TDRC in v2.4)
	TXXX("TXXX"), // User definied Text
	UFID("FileID"), // Unique File identifier
	WORS("WORS"),
	WXXX("Userdef.URL"), // User definied URL
	;
	
	private String name;
	
	ID3FrameType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}