/**
 *
 */
package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class TextFrameParser implements FrameParser {

	public enum Encoding {
		ISO_8859_1 ("ISO-8859-1"),
		UTF_16     ("UTF-16"),
		UTF_16BE   ("UTF-16BE"),
		UTF_8      ("UTF-8")
		;
		private Charset charset;
		Encoding(String charset) {
			this.charset = Charset.forName(charset);
		}
		public Charset getCharset() {
			return charset;
		}
	}

	private final static Logger logger = LogManager.getLogger("id3.frame");

	//-----------------------------------------------------------------
	public static String readUntilNullByte(ByteBuffer buffer, Encoding encoding, int maxPos) {
		if (logger.isTraceEnabled())
			logger.trace("Start reading from "+buffer.position()+" to "+maxPos+" with encoding "+encoding);
		int memorize = buffer.position();

		int lastNonNullPos = buffer.position();
		int current = buffer.position();
		while (current<maxPos) {
			byte tmp = buffer.get();
			if (logger.isTraceEnabled())
				logger.trace(current+"\t: Read "+tmp+" "+(char)tmp);
			if (encoding!=Encoding.ISO_8859_1 && encoding!=Encoding.UTF_8) {
				byte tmp2 = buffer.get();
				if (logger.isTraceEnabled()) {
					current++;
					logger.trace(current+"\t: Read "+tmp2+" "+(char)tmp2);
				}
			}
			// For ISO-8859-1, break at first null byte.
			// Other encodings require 2 null bytes
			if (tmp==0) {
//				if (encoding==Encoding.UTF_8) {
//					logger.fatal("Don't know if I should stop here!");
//					System.exit(0);
//				}
				break;
			} else {
				lastNonNullPos = buffer.position();
			}
			current = buffer.position();
		}
		int posAfterString = buffer.position();
		//		logger.debug(String.format("Read from %d to %d",memorize, lastNonNullPos));
		// Return to string start
		buffer.position(memorize);
		byte[] strDat = new byte[lastNonNullPos-memorize];
		buffer.get(strDat);

		buffer.position(posAfterString);

		String str = new String(strDat, encoding.getCharset());
		logger.debug("Read = ["+str+"]");
		//		System.exit(0);
		return (new String(strDat, encoding.getCharset())).trim();
	}

	//-----------------------------------------------------------------
	private static String readTextEncodedString(ByteBuffer buffer, int length) {
		Encoding encoding = Encoding.ISO_8859_1;
		try {
			encoding = Encoding.values()[buffer.get()];
		} catch (IllegalArgumentException e) {
		}

		return readUntilNullByte(buffer, encoding, buffer.position()+length-1);
//		byte[] data = new byte[length-1];
//		buffer.get(data);
//		return new String(data, encoding.getCharset());
	}

	//-----------------------------------------------------------------
	public static String[] readUserTextEncodedString(ByteBuffer buffer, int length) {
		logger.debug("parse from "+buffer.position()+" to "+(buffer.position()+length));
		int maxPos = buffer.position() + length;
		Encoding encoding = Encoding.ISO_8859_1;
		try {
			encoding = Encoding.values()[buffer.get()];
		} catch (IllegalArgumentException e) {
		}
		String desc = readUntilNullByte(buffer, encoding, maxPos);
		String val  = readUntilNullByte(buffer, encoding, maxPos);
		return new String[]{desc,val};

		//		byte[] data = new byte[length-1];
		//		buffer.get(data);
		//		logger.debug("Parse \n"+HexDumpMaker.makeHexDump(data));
		//		String desc = new String(data, encoding.getCharset());
		//			int offset = desc.getBytes(encoding.getCharset()).length;
		//			logger.debug("Offset = "+offset);
		//			logger.debug("Offset2 = "+desc.length());
		//			logger.debug("Offset3 = ["+desc+"]");
		//			offset+=2;
		//			String value = new String(data, offset, data.length-offset, encoding.getCharset());
		//			return new String[]{desc,value};
	}

	//-----------------------------------------------------------------
	/**
	 */
	public TextFrameParser() {
	}


	//-----------------------------------------------------------------
	/**
	 * @see org.prelle.myid3lib.frame.FrameParser#parse(java.lang.String, java.nio.ByteBuffer, int)
	 */
	@Override
	public ID3v2Frame parse(String frameID, ByteBuffer buffer, int length) {
		logger.debug("parse from "+buffer.position()+" to "+(buffer.position()+length));
		if (frameID.equals("TXXX")) {
			String[] pair = readUserTextEncodedString(buffer, length);
			return new UserDefinedTextFrame(frameID, pair[0], pair[1]);
		}

		String content = readTextEncodedString(buffer, length);

		if (frameID.equals("TRCK"))
			return new TrackFrame(frameID, content);

		return new TextFrame(frameID, content);
	}

}
