/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class TextFrame extends ID3v2Frame {

	private String text;
	
	//-----------------------------------------------------------------
	/**
	 */
	public TextFrame(String frameID, String content) {
		super(frameID);
		this.text = content;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return identifier+" = "+text;
	}

}
