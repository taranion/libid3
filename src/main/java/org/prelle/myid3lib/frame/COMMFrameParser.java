package org.prelle.myid3lib.frame;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.frame.TextFrameParser.Encoding;

/**
 * @author prelle
 *
 */
public class COMMFrameParser implements FrameParser {

	private final static Logger logger = LogManager.getLogger("id3.frame");

	//-----------------------------------------------------------------
	public COMMFrameParser() {
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.frame.FrameParser#parse(java.lang.String, java.nio.ByteBuffer, int)
	 */
	@Override
	public ID3v2Frame parse(String frameID, ByteBuffer buffer, int length) {
		int maxPos = buffer.position() + length;
		logger.debug("parse from "+buffer.position()+" to "+maxPos);

		Encoding encoding = Encoding.ISO_8859_1;
		try {
			encoding = Encoding.values()[buffer.get()];
		} catch (IllegalArgumentException e) {
		}
		logger.debug("COMM Desc encoding = "+encoding);
		byte[] lang_b = new byte[3];
		buffer.get(lang_b);
		String lang = new String(lang_b, Charset.forName("ISO-8859-1"));
		lang = lang.trim();

		logger.debug("lang = "+lang);
		String shortDesc = TextFrameParser.readUntilNullByte(buffer, encoding, buffer.remaining());
		logger.debug("shortDesc = "+shortDesc);
		String fullDesc = TextFrameParser.readUntilNullByte(buffer, encoding, buffer.remaining());
		logger.debug("fullDesc = "+fullDesc);

		COMMFrame ret = new COMMFrame(frameID);
		ret.setLanguage(lang);
		ret.setShortDescription(shortDesc);
		ret.setFullComment(fullDesc);
		return ret;
	}

}
