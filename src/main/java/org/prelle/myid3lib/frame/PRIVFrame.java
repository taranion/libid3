/**
 * 
 */
package org.prelle.myid3lib.frame;

/**
 * @author prelle
 *
 */
public class PRIVFrame extends ID3v2Frame {
	
	private String owner;
	private byte[] data;

	//-----------------------------------------------------------------
	/**
	 * @param frameID
	 */
	public PRIVFrame(String frameID) {
		super(frameID);
	}

	//-----------------------------------------------------------------
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	//-----------------------------------------------------------------
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	//-----------------------------------------------------------------
	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

}
