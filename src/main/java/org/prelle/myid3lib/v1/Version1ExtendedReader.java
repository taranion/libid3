package org.prelle.myid3lib.v1;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import org.prelle.myid3lib.Genre;
import org.prelle.myid3lib.ID3Version;

/**
 * @author prelle
 *
 */
public class Version1ExtendedReader {

	//-----------------------------------------------------------------
    private static boolean hasExtendedV1(final SeekableByteChannel file) throws IOException {
    	// File to small
    	if (file.size()<=(128+227))
    		return false;
    	
        // Check for extended v1 tag
        file.position(file.size() -128 -227);
        ByteBuffer buffer = ByteBuffer.allocate(4);
        file.read(buffer);
        String tag2 = new String(buffer.array(), 0, 4);
        if (tag2.equals("TAG+")) {
        	System.err.println("Version1Reader.detectV1Version: Extended v1 tag found");
        	System.exit(0);
        	return false;
        }
        	
        file.position(file.size() - 125);
        return true;
    }

	//-----------------------------------------------------------------
	public static Version1Header read(Path path) throws IOException {
		SeekableByteChannel file = Files.newByteChannel(path);
		if (!hasExtendedV1(file))
			return null;
		
		Version1Header header = new Version1Header(ID3Version.V0);
		byte[] data = new byte[223];
    	ByteBuffer buffer = ByteBuffer.wrap(data);
        file.read(buffer);
        
        header.setTitle( new String(data, 0,60, "ISO-8859-1").trim() );
        buffer.clear();
        
        file.read(buffer);
        header.setArtist( new String(data,60,60, "ISO-8859-1").trim() );
        buffer.clear();
        
        file.read(buffer);
        header.setAlbum( new String(data,120,60, "ISO-8859-1").trim() );
        buffer.clear();
        
//        byte speed = data[180];
        String genre     = new String(data,181,30, "ISO-8859-1").trim();
//        String startTime = new String(data,211, 6, "ISO-8859-1").trim();
//        String endTime   = new String(data,217, 6, "ISO-8859-1").trim();
        try {
			header.setGenre(Genre.valueOf(genre.toUpperCase()));
		} catch (Exception e) {
		}
        buffer.clear();

        return header;
	}

}
