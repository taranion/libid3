package org.prelle.myid3lib.v1;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.prelle.myid3lib.Genre;
import org.prelle.myid3lib.ID3Version;

/**
 * @author prelle
 *
 */
public class Version1Reader {

	//-----------------------------------------------------------------
    private static ID3Version detectV1Version(final SeekableByteChannel file) throws IOException {
    	// File to small
    	if (file.size()<=128)
    		return null;

    	ID3Version detected = null;

        // If there is a tag, it is 127 bytes long and we'll find the tag
        file.position(file.size() - 128);
        ByteBuffer buffer = ByteBuffer.allocate(3);

        // read the TAG value
        file.read(buffer);
        final String tag = new String(buffer.array(), 0, 3);
        if (!tag.equals("TAG"))
        	return null;
    	detected = ID3Version.V0;

        // Check for the empty byte before the TRACK
    	buffer.clear();
        file.position(file.size() - 3);
        file.read(buffer);
        if (buffer.array()[0] == 0)
            detected = ID3Version.V1;

        // Check for extended v1 tag
        file.position(file.size() -128 -227);
        buffer = ByteBuffer.allocate(4);
        file.read(buffer);
        String tag2 = new String(buffer.array(), 0, 4);
        if (tag2.equals("TAG+")) {
        	System.err.println("Version1Reader.detectV1Version: Extended v1 tag found");
        	System.exit(0);
        }

        file.position(file.size() - 125);
        return detected;
    }

	//-----------------------------------------------------------------
	public static Version1Header read(Path path) throws IOException {
		SeekableByteChannel file = Files.newByteChannel(path);
		ID3Version version = detectV1Version(file);
		if (version==null)
			return null;

		Version1Header header = new Version1Header(version);
    	ByteBuffer buffer = ByteBuffer.allocate(30);

        file.read(buffer);
        header.setTitle( new String(buffer.array(), 0, 30, "ISO-8859-1").trim() );
        buffer.clear();

        file.read(buffer);
        header.setArtist( new String(buffer.array(), "ISO-8859-1").trim() );
        buffer.clear();

        file.read(buffer);
        header.setAlbum( new String(buffer.array(), 0, 30, "ISO-8859-1").trim() );
        buffer.clear();

        buffer = ByteBuffer.allocate(4);
        file.read(buffer);
        try {
			header.setYear(Integer.parseInt(new String(buffer.array(), 0, 4, "ISO-8859-1").trim()) );
		} catch (NumberFormatException e1) {
		}
        buffer.clear();

        buffer = ByteBuffer.allocate(30);
        file.read(buffer);
        if (version==ID3Version.V0) {
            header.setComment( new String(buffer.array(), 0, 30, "ISO-8859-1").trim() );
        } else {
            header.setComment( new String(buffer.array(), 0, 28, "ISO-8859-1").trim() );
            header.setTrack(buffer.array()[29]);

        }
        buffer.clear();

        file.read(buffer);
        byte genre = buffer.array()[0];
        int idx = (genre<0)?(255+genre):genre;
        try {
			header.setGenre( Genre.values()[idx]);
		} catch (ArrayIndexOutOfBoundsException e) {
			LogManager.getLogger("id3").warn("Unknown V1 genre: "+idx);
		}

        file.close();
        return header;
	}

}
