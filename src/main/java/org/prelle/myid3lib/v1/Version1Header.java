/**
 * 
 */
package org.prelle.myid3lib.v1;

import org.prelle.myid3lib.AbstractID3Header;
import org.prelle.myid3lib.Genre;
import org.prelle.myid3lib.ID3Version;


/**
 * @author prelle
 *
 */
public class Version1Header extends AbstractID3Header {

	private String title;
	private String artist;
	private String album;
	private int year;
	private String comment;
	private Genre genre;
	private int track;
	
	//-----------------------------------------------------------------
	/**
	 */
	public Version1Header(ID3Version version) {
		super(version);
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return String.format("%s: title='%s' artist='%s' album='%s', year=%d comment='%s', genre=%s, track=%d", version.getName(), title, artist, album, year,comment,genre,track);
	}

	//-----------------------------------------------------------------
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	//-----------------------------------------------------------------
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}

	//-----------------------------------------------------------------
	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the album
	 */
	public String getAlbum() {
		return album;
	}

	//-----------------------------------------------------------------
	/**
	 * @param album the album to set
	 */
	public void setAlbum(String album) {
		this.album = album;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	//-----------------------------------------------------------------
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	//-----------------------------------------------------------------
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the genre
	 */
	public Genre getGenre() {
		return genre;
	}

	//-----------------------------------------------------------------
	/**
	 * @param genre the genre to set
	 */
	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the track
	 */
	public int getTrack() {
		return track;
	}

	//-----------------------------------------------------------------
	/**
	 * @param track the track to set
	 */
	public void setTrack(int track) {
		this.track = track;
	}

}
