/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 * 
 * @see "href:http://de.wikipedia.org/wiki/Liste_der_ID3v1-Genres"
 */
public enum Genre {

	BLUES,
	CLASSIC_ROCK,
	COUNTRY,
	DANCE,
	DISCO,
	FUNK, // 5
	GRUNGE, 
	HIP_HOP,
	JAZZ,
	METAL,
	NEW_AGE, // 10
	OLDIES,
	OTHER,
	POP,
	RYTHM_AND_BLUES,
	RAP, // 15
	REGGAE,
	ROCK,
	TECHNO,
	INDUSTRIAL,
	ALTERNATIVE, // 20
	SKA, 
	DEATH_METAL,
	PRANKS,
	SOUNDTRACK, 
	EURO_TECHNO, // 25
	AMBIENT,
	TRIP_HOP,
	VOCAL,
	JAZZ_AND_FUNK,
	FUSION, // 30
	TRANCE,
	CLASSICAL,
	INSTRUMENTAL,
	ACID,
	HOUSE, // 35
	GAME,
	SOUND_CLIP,
	GOSPEL,
	NOISE,
	ALTERNATIVE_ROCK, // 40
	BASS,
	SOUL,
	PUNK,
	SPACE,
	MEDIATIVE, // 45
	INSTRUMENTAL_POP,
	INSTRUMENTAL_ROCK,
	ETHNIC,
	GOTHIC,
	DARKWAVE, // 50
	TECHNO_INDUSTRIAL,
	ELECTRONIC,
	POP_FOLK,
	EURODANCE,
	DREAM, // 55
	SOUTHERN_ROCK,
	COMEDY,
	CULT,
	GANGSTA,
	TOP_40, // 60
	CHRISTIAN_RAP,
	POP_FUNK,
	JUNGLE,
	NATIVE_US,
	CABARET, // 65
	NEW_WAVE,
	PSYCHEDELIC,
	RAVE,
	SHOWTUNES,
	TRAILER, // 70
	LO_FI,
	TRIBAL,
	ACID_PUNK,
	ACID_JAZZ,
	POLKA, // 75
	RETRO,
	MUSICAL,
	ROCK_N_ROLL,
	HARD_ROCK,
	FOLK, // 80
	FOLK_ROCK,
	NATIONAL_FOLK,
	SWING,
	FAST_FUSION,
	BEBOP, // 85
	LATIN,
	REVIVAL,
	CELTIC,
	BLUEGRASS,
	AVANTGARDE, // 90
	GOTHIC_ROCK,
	PROGRESSIVE_ROCK,
	PSYCHEDELIC_ROCK,
	SYMPHONIC_ROCK,
	SLOW_ROCK, // 95
	BIG_BAND,
	CHORUS,
	EASY_LISTENING,
	ACOUSTIC,
	;

}
