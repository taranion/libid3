/**
 * 
 */
package org.prelle.myid3lib;

import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class Bitrate {

	private static Map<MPEGVersion, Map<MPEGLayer, int[]>> data = new HashMap<MPEGVersion, Map<MPEGLayer,int[]>>();

	static {
		int[] v1l1 = new int[]{0,32,64,96,128,160,192,224,256,288,320,352,384,416,448}; 
		int[] v1l2 = new int[]{0,32,48,56,64,80,96,112,128,160,192,224,256,320,384}; 
		int[] v1l3 = new int[]{0,32,40,48,56,64,80,96,112,128,160,192,224,256,320}; 
		Map<MPEGLayer,int[]> v1Map = new HashMap<>();
		v1Map.put(MPEGLayer.LAYER_1, v1l1);
		v1Map.put(MPEGLayer.LAYER_2, v1l2);
		v1Map.put(MPEGLayer.LAYER_3, v1l3);
		
		data.put(MPEGVersion.VERSION_1, v1Map);
	}
	
	//-----------------------------------------------------------------
	public static int getBitrateKBit(MPEGVersion version, MPEGLayer layer, int brIndex) {
		try {
			if (!data.containsKey(version))
				return -1;
			return data.get(version).get(layer)[brIndex];
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
}
