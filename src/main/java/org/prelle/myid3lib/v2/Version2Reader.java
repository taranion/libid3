package org.prelle.myid3lib.v2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.BitSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.HexDumpMaker;
import org.prelle.myid3lib.ID3Version;
import org.prelle.myid3lib.TagException;
import org.prelle.myid3lib.frame.FrameParserRegistry;
import org.prelle.myid3lib.frame.ID3v2Frame;

/**
 * @author prelle
 *
 */
/**
 * @author prelle
 *
 */
public class Version2Reader {

	protected final static Logger logger = LogManager.getLogger("id3");
	private final static Logger frameLogger = LogManager.getLogger("id3.frame");

	private static BitSet SYNCHEADER = new BitSet(32);

	//-----------------------------------------------------------------
	static {
		SYNCHEADER.set(0, 11);
	}

	//-----------------------------------------------------------------
    private static ID3Version detectV2Version(final SeekableByteChannel file) throws IOException {
    	//-----INSERTED
    	ByteBuffer bla2 = ByteBuffer.allocate(16);
    	file.read(bla2);
    	bla2.position(0);
    	logger.debug("Tag-Header: "+HexDumpMaker.makeHexDump(bla2.array()));
    	//-----

    	ID3Version detected = null;

        // Expect header at the start of the file
        file.position(0);
        ByteBuffer buffer = ByteBuffer.allocate(5);

        // read the TAG value
        file.read(buffer);
        final String tag = new String(buffer.array(), 0, 3);
        if (!tag.equals("ID3"))
        	return null;
    	detected = ID3Version.V2;

    	byte major = buffer.get(3);
    	byte minor = buffer.get(4);
    	switch (major) {
    	case 2:
    		detected = ID3Version.V2;
    		break;
    	case 3:
    		detected = ID3Version.V3;
    		break;
    	case 4:
    		detected = ID3Version.V4;
    		break;
    	default:
    		logger.warn("Unsupported ID Version: 2."+major+"."+minor);
    		return null;
    	}
    	logger.debug("Detected "+detected);

        return detected;
    }

	//-----------------------------------------------------------------
    public static String dumpNextBytes(ByteBuffer buffer) {
    	int memorize = buffer.position();
    	byte[] data = new byte[Math.min(16, buffer.remaining())];
    	buffer.get(data);
    	String ret = HexDumpMaker.makeHexDump(data);
    	buffer.position(memorize);
    	return ret;
    }

	//-----------------------------------------------------------------
	public static Version2Header readTagHeader(SeekableByteChannel file) throws IOException, TagException {
//		long tagStartPos = file.position();
		ID3Version version = detectV2Version(file);
		if (version==null)
			return null;

		Version2Header header = new Version2Header(version);
		TagParser parser = null;
		switch (version) {
		case V2:
			parser = new V22TagParser();
			break;
		case V3:
		case V4:
			parser = new V23TagParser();
			break;
		default:
			logger.warn("ID3 version not supported: "+version);
			return header;
		}
		parser.setSyncSafeIntParsing(true);
		header.setParser(parser);

		/*
		 * Read the remaining bytes of the tag header into a buffer.
		 */
        file.position(5);
    	ByteBuffer tagHeadBuffer = ByteBuffer.allocate(5);
        file.read(tagHeadBuffer);
        tagHeadBuffer.position(0);

        // Read flags
    	header.setFlags(parser.readTagFlags(tagHeadBuffer));
    	//-------------- Debug flags----------------
    	BitSet flags = header.getFlags();
    	boolean unsync   = flags.get(Version2Header.Flags.UNSYNCHRONISATION.ordinal());
    	logger.debug("Header Flags = "+flags);
//    	boolean extended = flags.get(Version2Header.Flags.EXTENDED.ordinal());
//    	boolean experimental = flags.get(Version2Header.Flags.EXPERIMENTAL.ordinal());
//    	boolean footer = flags.get(Version2Header.Flags.FOOTER_PRESENT.ordinal());
    	if (logger.isTraceEnabled()) {
    		int lastSet = 0;
    		for (int i=0; i<flags.cardinality(); i++) {
    			int flag = flags.nextSetBit(lastSet);
    			logger.trace("Flag "+Version2Header.Flags.values()[flag]+" is set");
    			lastSet = flag;
    		}
    	}
    	if (unsync) {
    		logger.error("Unsynchronized tag discovered");
    		throw new RuntimeException("Unsynchronized tag discovered");
    	}
    	//-------------- Debug flags----------------


    	// Memorize the buffer position before tag size
    	int beforeTagSize = tagHeadBuffer.position();
    	int size=0;
    	// Tracks whether non-syncsafe integers have already been tested
    	boolean triedNonSyncSafe = false;

    	// Size

    	do {
    		size = parser.readTagSize(tagHeadBuffer);
    		header.setTagSize(size+10);

    		logger.debug("Size = "+size);
    		// After reading the whole tag, there should be an MPEG
    		// header following
    		file.position(size+10);
    		if (!checkMPGHeader(file)) {
    			// Tag size must have been wrong. Switch parser
    			// to non-sync-safe
    			tagHeadBuffer.position(beforeTagSize);
    			if (!triedNonSyncSafe) {
    				logger.warn("Tag is not followed by MPEG header - assume non-sync safe integers");
    				parser.setSyncSafeIntParsing(false);
    				triedNonSyncSafe = true;
    				continue;
    			} else {
    				logger.warn("Did not find MPEG header - neither with sync-safe tag size or not");
    				parser.setSyncSafeIntParsing(true);
    				break;
    			}
    		} else {
    			logger.debug("Tag size (without header) is confirmed to be "+size+" (non-syncsafe = "+triedNonSyncSafe+")");
    			header.setTagSize(size+10);
    			break;
    		}
    	} while (true);

    	logger.debug("Assume sync safe sizes = "+parser.getSyncSafeIntParsing());

    	tagHeadBuffer.clear();
    	return header;
	}

	//-----------------------------------------------------------------
	public static Version2Header read(Path path) throws IOException, TagException {
		SeekableByteChannel file = Files.newByteChannel(path);

		Version2Header header = readTagHeader(file);
		if (header==null) {
			// No V2 header
			return null;
		}

		logger.debug("Version "+header.getVersion()+" frames start at 10");
		logger.debug("MPG starts at "+header.getTagSize());
		readFrames(file, header);
		file.close();

        return header;
	}

	//-----------------------------------------------------------------
	private static void readFrames(SeekableByteChannel file, Version2Header header) throws TagException, IOException {
    	/**
    	 * Now parse frames
    	 */
		// Start reading all frames into a buffer
		ByteBuffer buffer = ByteBuffer.allocateDirect(header.getTagSize()-10);
		file.position(10);
		file.read(buffer);
		buffer.position(0);
//    	long pos = buffer.position();
//    	logger.debug("Start parsing frame at "+pos);

		TagParser parser = header.getParser();
    	while (buffer.remaining()>0) {
    		// Memorize current start position
    		int start = buffer.position();
    		// Ensure we have not reached padding yet
    		int checkB = buffer.get();
    		if (checkB==0) {
    			// Reached padding
    			buffer.position(start);
    			break;
    		}
			buffer.position(start);

    		FrameToParse toParse = new FrameToParse();
    		toParse.startPos = buffer.position();

        	//-----INSERTED
    		if (logger.isTraceEnabled()) {
    			byte[] bla3 = new byte[Math.min(15, buffer.remaining())];
    			buffer.get(bla3);
    			logger.trace("Frame header: "+HexDumpMaker.makeHexDump(bla3));
    			buffer.position(start);
    		}
    		//-----
    		// Ensure that there are enough bytes for another frame left
    		if (buffer.remaining()<7)
    			break;

    		// Read frame name
    		String frameID   = parser.readFrameID(buffer);
    		int beforeFrameSize = buffer.position();
    		int frameSize    = parser.readFrameSize(buffer);
    		BitSet frameFlags= parser.readFrameFlags(buffer);
    		int afterBitSet = buffer.position();
    		logger.debug("Framesize of "+frameID+" is "+frameSize);
    		if (frameSize==0)
    			break;
    		/*
    		 * Verify frame size by checking if the next frame
    		 * (if there is one) starts with a letter in the name.
    		 */
    		//-----BEGIN---VERIFY-FRAME-SIZE---------
    		buffer.position(afterBitSet+frameSize);
    		if (buffer.remaining()>4) {
    			logger.debug("Next Frame would start with "+dumpNextBytes(buffer));
    			byte[] tmp4 = new byte[4];
    			buffer.get(tmp4);
    			String tmp = new String(tmp4, Charset.forName("US-ASCII"));
    			char ch = tmp.charAt(0);
//    			char ch = buffer.getChar();
    			logger.debug("ch = "+ch);
    			if (ch==0 || (Character.isLetter(ch) && Character.isUpperCase(ch)))
    				logger.debug("Seems to be ok");
    			else {
    				logger.debug(frameSize+ " does not seem to be an valid frame ID");
    				buffer.position(beforeFrameSize);
    				frameSize    = parser.readFrameSizeOtherWay(buffer);
    				logger.debug("Does not seem to be an valid frame ID - could be "+frameSize);
    				buffer.position(afterBitSet);
    			}
    		}
    		buffer.position(afterBitSet);
    		//-----END-----VERIFY-FRAME-SIZE---------


    		if (buffer.remaining()<frameSize) {
        			logger.warn("Frame "+frameID+" has an invalid frame size. Expect "+frameSize+" bytes for the frame, but there are only "+buffer.remaining()+" left");
        			throw new TagException("Expect "+frameSize+" bytes for the frame, but there are only "+buffer.remaining()+" left");
    		}

    		// Now make a copy of the whole block (incl. header)
    		byte[] frameBytes = new byte[frameSize];
    		buffer.get(frameBytes);
    		// Call parser for new frame
    		ID3v2Frame frame = parseFrame(parser, frameID, frameFlags, frameBytes);
    		logger.info("Parsed "+frame+"  of type "+frame.getClass());
    		header.addFrame(frame);

    	}
		buffer.clear();
	}

	//-----------------------------------------------------------------
	// Returns a bitset containing the values in bytes.
	// The byte-ordering of bytes must be big-endian which means the most significant bit is in element 0.
	public static BitSet fromByteArray(byte[] bytes) {
	    BitSet bits = new BitSet();
	    for (int i=0; i<bytes.length*8; i++) {
	        if ((bytes[i/8]&(1<<(i%8))) > 0) {
	            bits.set(i);
	        }
	    }
	    return bits;
	}

	//-----------------------------------------------------------------
	/**
	 * Check if there is a MPEG header at the current file position
	 */
	private static boolean checkMPGHeader(SeekableByteChannel file) throws IOException {
		byte[] mpgFrame = new byte[4];
		ByteBuffer buffer = ByteBuffer.wrap(mpgFrame);
		file.read(buffer);

		logger.debug("CheckMPG = "+HexDumpMaker.makeHexDump(mpgFrame));
		BigEndianBitSet syncHeader = new BigEndianBitSet(mpgFrame);
		return syncHeader.matches(SYNCHEADER, 0, 10);
	}

//	//-----------------------------------------------------------------
//	private static String textTagReader(byte[] content, BitSet flags) {
//    	byte encoding = content[0];
//    	switch (encoding) {
//    	case 0: // ISO-8859-1
//    		logger.debug("iso-8859-1");
//    		return new String(content, 1, content.length-1, Charset.forName("ISO-8859-1"));
//    	case 1: /* UTF-16 [UTF-16] encoded Unicode [UNICODE] with BOM. All
//        strings in the same frame SHALL have the same byteorder. */
//    		logger.debug("UTF-16 with BOM");
//    		return new String(content, 1, content.length-1, Charset.forName("UTF-16"));
//    	case 2: /* UTF-16BE [UTF-16] encoded Unicode [UNICODE] without BOM. */
//    		logger.debug("UTF-16 without BOM");
//    		return new String(content, 1, content.length-1, Charset.forName("UTF-16BE"));
//    	case 3: /* UTF-8 */
//    		logger.debug("UTF-8");
//    		return new String(content, 1, content.length-1, Charset.forName("UTF-8"));
//    	default:
//    		logger.warn("No valid encoding form text frame.");
//        	logger.debug("text content = \n"+HexDumpMaker.makeHexDump(content));
//    		return new String(content, Charset.forName("ISO-8859-1"));
//    	}
//	}

	//-----------------------------------------------------------------
	private static ID3v2Frame parseFrame(TagParser parser, String frameID, BitSet frameFlags, byte[] bytes) {
		if (logger.isTraceEnabled()) {
			frameLogger.trace("------Parse "+frameID+" frame----------------------------");
			HexDumpMaker.makeHexDump(bytes, frameLogger);
		}

		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		logger.debug(String.format("Frame %s is %d bytes long and has the flags %s", frameID, bytes.length, frameFlags.toString()));
		if (!frameFlags.isEmpty()) {
			logger.warn("Flags = "+frameFlags.toString());
			for (int i=0; i<frameFlags.size(); i++) {
				if (frameFlags.get(i))
				logger.warn(FrameFlags.getForBitFromBitSet(i));
			}

			logger.warn(String.format("Frame %s has flags %s", frameID, frameFlags.toString()));
//			logger.fatal("Frame is "+FrameParserRegistry.parse(frameID, buffer, bytes.length));
//			System.exit(0);
		}

		return FrameParserRegistry.parse(frameID, buffer, bytes.length);
	}

//	//-----------------------------------------------------------------
//	/**
//	 * @param buffer
//	 * @param length
//	 * @return
//	 */
//	private static String readTextEncodedString(ByteBuffer buffer, int length) {
//		byte encoding = buffer.get();
//		String charset = "ISO-8859-1";
//		switch (encoding) {
//    	case 0: // ISO-8859-1
//    		charset =  "ISO-8859-1";
//    		break;
//    	case 1: /* UTF-16 [UTF-16] encoded Unicode [UNICODE] with BOM. All
//        strings in the same frame SHALL have the same byteorder. */
//    		charset = "UTF-16";
//    		break;
//    	case 2: /* UTF-16BE [UTF-16] encoded Unicode [UNICODE] without BOM. */
//    		charset = "UTF-16BE";
//    		break;
//    	case 3: /* UTF-8 */
//    		charset = "UTF-8";
//    		break;
//    	default:
//    		logger.warn("No valid encoding form text frame.");
//    	}
//
//		byte[] data = new byte[length-1];
//		buffer.get(data);
//		return new String(data, Charset.forName(charset));
//	}
}

class FrameToParse {
	long startPos;
	long frameSize;
	String frameID;
	BitSet frameFlags;
}
