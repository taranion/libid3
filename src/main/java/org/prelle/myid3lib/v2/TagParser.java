/**
 * 
 */
package org.prelle.myid3lib.v2;

import java.nio.ByteBuffer;
import java.util.BitSet;

/**
 * @author prelle
 *
 */
public interface TagParser {
	
	public void setSyncSafeIntParsing(boolean isSyncSafe);
	public boolean getSyncSafeIntParsing();
	
	public BitSet readTagFlags(ByteBuffer buffer);

	public int readTagSize(ByteBuffer buffer);

	public String readFrameID(ByteBuffer buffer);
	
	public int readFrameSize(ByteBuffer buffer);
	
	public int readFrameSizeOtherWay(ByteBuffer buffer);

	public BitSet readFrameFlags(ByteBuffer buffer);

	public String readEncodedString(ByteBuffer buffer);
	
}
