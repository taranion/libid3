/**
 *
 */
package org.prelle.myid3lib.v2;

import java.nio.ByteBuffer;
import java.util.BitSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class V22TagParser implements TagParser {

	protected final static Logger logger = LogManager.getLogger("id3");

	protected static BitSet MASK4 = new BitSet(32);
	protected static BitSet MASK3 = new BitSet(24);

	protected boolean isSyncSafe = true;

	//-----------------------------------------------------------------
	static {
		MASK4.set(7);
		MASK4.set(15);
		MASK4.set(23);
		MASK4.set(31);

		MASK3.set(7);
		MASK3.set(15);
		MASK3.set(23);
	}

	//-----------------------------------------------------------------
	protected static int readByteAsInt(ByteBuffer buffer) {
		int b = buffer.get();
		if (b<0) {
			b = 256+b;
		}
		return b;
	}

	//-----------------------------------------------------------------
	protected static byte read7BitByte(ByteBuffer buffer) {
		int b = readByteAsInt(buffer);
		b %= 127;
		return (byte)b;
	}

	//-----------------------------------------------------------------
	public V22TagParser() {
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#setSyncSafeIntParsing(boolean)
	 */
	@Override
	public void setSyncSafeIntParsing(boolean isSyncSafe) {
		this.isSyncSafe = isSyncSafe;
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readTagFlags(java.nio.ByteBuffer)
	 */
	@Override
	public BitSet readTagFlags(ByteBuffer buffer) {
		byte[] copy = new byte[1];
		buffer.get(copy);
		return BitSet.valueOf(copy);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readTagSize(java.nio.ByteBuffer)
	 */
	@Override
	public int readTagSize(ByteBuffer buffer) {
		int start = buffer.position();
		// Read not sync safe
		int nonSyncSafe = buffer.getInt();
		buffer.position(start);

		byte[] data = new byte[4];
		buffer.get(data);
		BitSet set = BitSet.valueOf(data);
		if (set.intersects(MASK4)) {
			// Size was definitely not written sync safe
			return nonSyncSafe;
		}
		// Size may be given sync safe
		// To be sure, return the maximum
		int syncSafe = (data[0]<<21) + (data[1]<<14) + (data[2]<<7) + data[3];
		if (isSyncSafe)
			return Math.min(syncSafe, nonSyncSafe);
		else
			return Math.max(syncSafe, nonSyncSafe);

//		int size = read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		return size;
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameID(java.nio.ByteBuffer)
	 */
	@Override
	public String readFrameID(ByteBuffer buffer) {
		byte[] data = new byte[3];
		buffer.get(data);
		return new String(data);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameSize(java.nio.ByteBuffer)
	 */
	@Override
	public int readFrameSize(ByteBuffer buffer) {
		int start = buffer.position();
		byte[] data = new byte[3];
		buffer.get(data);
		BitSet set = BitSet.valueOf(data);
		if (set.intersects(MASK3)) {
			// Size was not written sync safe
			buffer.position(start);
			return (readByteAsInt(buffer)<<16) + (readByteAsInt(buffer)<<8) + readByteAsInt(buffer);
		}
		// Was given possibly sync safe
		int syncSafe = (data[0]<<14) + (data[1]<<7) + data[2];
		int nonSyncSafe = (data[0]<<14) + (data[1]<<7) + data[2];
		if (isSyncSafe)
			return Math.min(syncSafe, nonSyncSafe);
		else
			return Math.max(syncSafe, nonSyncSafe);
//		int size = read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		return new Size(size,-1);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameSize(java.nio.ByteBuffer)
	 */
	@Override
	public int readFrameSizeOtherWay(ByteBuffer buffer) {
		int start = buffer.position();
		byte[] data = new byte[3];
		buffer.get(data);
		BitSet set = BitSet.valueOf(data);
		if (set.intersects(MASK3)) {
			// Size was not written sync safe
			buffer.position(start);
			return buffer.getInt();
		}
		// Was given possibly sync safe
		int syncSafe = (data[0]<<14) + (data[1]<<7) + data[2];
		int nonSyncSafe = (data[0]<<14) + (data[1]<<7) + data[2];
		if (isSyncSafe)
			return Math.max(syncSafe, nonSyncSafe);
		else
			return Math.min(syncSafe, nonSyncSafe);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameFlags(java.nio.ByteBuffer)
	 */
	@Override
	public BitSet readFrameFlags(ByteBuffer buffer) {
		return new BitSet();
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readEncodedString(java.nio.ByteBuffer)
	 */
	@Override
	public String readEncodedString(ByteBuffer buffer) {
		return null;
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#getSyncSafeIntParsing()
	 */
	@Override
	public boolean getSyncSafeIntParsing() {
		return isSyncSafe;
	}

}
