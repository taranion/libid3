package org.prelle.myid3lib.v2;

import java.util.BitSet;

public class BigEndianBitSet {

	private BitSet bits;
	
	public BigEndianBitSet(int size) {
		bits = new BitSet(size);
	}
	
	public BigEndianBitSet(byte[] bytes) {
		bits = new BitSet();
		int bitlen = bytes.length*8;
	    for (int i=0; i<bitlen; i++) {
	        if ((bytes[bytes.length-i/8-1]&(1<<(i%8))) > 0) {
	            bits.set(bitlen-i-1);
	        }
	    }
	}
	public String toString() {
		return bits.toString();
	}
	public int getAsInt(int fromIndex, int toIndex) {
		int ret = 0;
		for (int i=fromIndex; i<=toIndex; i++) {
			ret <<= 1;
			if (bits.get(i)) {
				ret++;
			}
		}
		return ret;
	}
	public void set(int fromIndex, int toIndex) {
		bits.set(fromIndex, toIndex);
	}
	public boolean matches(BitSet other, int fromIndex, int toIndex) {

		for (int i=fromIndex; i<=toIndex; i++) {
			if (bits.get(i)!=other.get(i)) return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		byte[] data = new byte[]{(byte)0xff, (byte)0xfb, (byte)0x92, (byte)0x04};
		BigEndianBitSet set = new BigEndianBitSet(data);
		System.out.println(set);
		System.out.println("28-31 = "+set.getAsInt(28, 31));
		System.out.println("16-19 = "+set.getAsInt(16, 19));
	}
}
