/**
 * 
 */
package org.prelle.myid3lib.v2;

import java.nio.ByteBuffer;
import java.util.BitSet;

/**
 * @author prelle
 *
 */
public class V23TagParser extends V22TagParser implements TagParser {

	//-----------------------------------------------------------------
	public V23TagParser() {
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameID(java.nio.ByteBuffer)
	 */
	@Override
	public String readFrameID(ByteBuffer buffer) {
		byte[] data = new byte[4];
		buffer.get(data);
		return new String(data);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameSize(java.nio.ByteBuffer)
	 */
	@Override
	public int readFrameSize(ByteBuffer buffer) {
		return readTagSize(buffer);
//		byte[] fourBytes = null;
//		int start = buffer.position();
//		if (logger.isTraceEnabled()) {
//			fourBytes = new byte[4];
//			buffer.get(fourBytes);
//			buffer.position(start);
//		}
//		
//		int bytesToRead = 4;
//		int size = 0;
//		boolean wasSyncSafe=true;
//		while (bytesToRead>0) {
//			size <<= 7;
//			byte b = buffer.get();
//			if (b<0) {
//				// Read bytes was not-syncsafe
//				wasSyncSafe = false;
//				break;
//			}
//			size += b;
//			bytesToRead--;
//		}
//		
//		if (wasSyncSafe)
//			return new Size(size, -1);
//		
//		// Not sync safe
//		buffer.position(start);
//		return new Size(-1,buffer.getInt());
//		int size = read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		return size;
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.V22TagParser#readFrameSizeOtherWay(java.nio.ByteBuffer)
	 */
	@Override
	public int readFrameSizeOtherWay(ByteBuffer buffer) {
		int start = buffer.position();
		// Read not sync safe
		int nonSyncSafe = buffer.getInt();
		buffer.position(start);
		
		byte[] data = new byte[4];
		buffer.get(data);
		BitSet set = BitSet.valueOf(data);
		if (set.intersects(MASK4)) {
			// Size was definitely not written sync safe
			return nonSyncSafe;
		}
		// Size may be given sync safe
		// To be sure, return the maximum
		int syncSafe = (data[0]<<21) + (data[1]<<14) + (data[2]<<7) + data[3];		
		if (isSyncSafe)
			return Math.max(syncSafe, nonSyncSafe);
		else
			return Math.min(syncSafe, nonSyncSafe);		
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameSize(java.nio.ByteBuffer)
	 */
//	@Override
//	public int readFrameSizeSyncSafe(ByteBuffer buffer) {
//		int size = read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		size <<= 7;
//		size += read7BitByte(buffer);
//		return size;
//	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readFrameFlags(java.nio.ByteBuffer)
	 */
	@Override
	public BitSet readFrameFlags(ByteBuffer buffer) {
		byte[] copy = new byte[2];
		buffer.get(copy);
//		System.out.println("FrameFlags = "+HexDumpMaker.makeHexDump(copy));
		return BitSet.valueOf(copy);
	}

	//-----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.myid3lib.v2.TagParser#readEncodedString(java.nio.ByteBuffer)
	 */
	@Override
	public String readEncodedString(ByteBuffer buffer) {
		return null;
	}

}
