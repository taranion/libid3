/**
 * 
 */
package org.prelle.myid3lib.v2;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.prelle.myid3lib.AbstractID3Header;
import org.prelle.myid3lib.ID3Version;
import org.prelle.myid3lib.frame.ID3FrameType;
import org.prelle.myid3lib.frame.ID3v2Frame;
import org.prelle.myid3lib.frame.TextFrame;
import org.prelle.myid3lib.frame.UserDefinedTextFrame;

/**
 * @author prelle
 *
 */
public class Version2Header extends AbstractID3Header {

	enum Flags {
		UNKNOWN0,
		UNKNOWN1,
		UNKNOWN2,
		UNKNOWN3,
		FOOTER_PRESENT, // Since 2.4
		EXPERIMENTAL, // Since 2.3
		EXTENDED, // Extended Header present
		UNSYNCHRONISATION
	}
	
	private Map<String, List<ID3v2Frame>> framesByID;
	private int tagSize;
	private BitSet flags;
	private TagParser parser;
	
	//-----------------------------------------------------------------
	/**
	 */
	public Version2Header(ID3Version version) {
		super(version);
		framesByID = new HashMap<String, List<ID3v2Frame>>();
	}
	
	//-----------------------------------------------------------------
	public String toString() {
		return version.getName()+" = "+framesByID;
	}
	
	//-----------------------------------------------------------------
	void addFrame(ID3v2Frame frame) {
		String key = frame.getIdentifier();
		List<ID3v2Frame> list = framesByID.get(key);
		if (list==null) {
			list = new ArrayList<ID3v2Frame>();
			framesByID.put(key, list);
		}
		list.add(frame);
	}

	//-----------------------------------------------------------------
	public boolean has(String type) {
		return framesByID.containsKey(type) && !framesByID.get(type).isEmpty();
	}

	//-----------------------------------------------------------------
	public boolean has(ID3FrameType type) {
		return has(type.name());
	}

	//-----------------------------------------------------------------
	public ID3v2Frame get(String type) {
		List<ID3v2Frame> list = framesByID.get(type);
		if (list==null || list.isEmpty()) return null;
		return list.get(0);
	}

	//-----------------------------------------------------------------
	public ID3v2Frame get(ID3FrameType type) {
		return get(type.name());
	}

	//-----------------------------------------------------------------
	public String getAsString(String type) {
		List<ID3v2Frame> list = framesByID.get(type);
		if (list==null || list.isEmpty()) return null;
		ID3v2Frame frame = list.get(0);
		if (frame instanceof TextFrame)
			return ((TextFrame)frame).getText();
		return list.get(0).toString();
	}

	//-----------------------------------------------------------------
	public String getAsString(ID3FrameType type) {
		return getAsString(type.name());
	}

//	//-----------------------------------------------------------------
//	protected int getAsInt(InfoType type) {
//		List<ID3v2Frame> list = framesByID.get(type);
//		if (list==null || list.isEmpty()) return -1;
//		return (Integer)list.get(0).getData();
//	}
//
//	//-----------------------------------------------------------------
//	protected ID3Date getAsDate(InfoType type) {
//		List<ID3v2Frame> list = framesByID.get(type);
//		if (list==null || list.isEmpty()) return null;
//		return (ID3Date)list.get(0).getData();
//	}

	//-----------------------------------------------------------------
	public List<ID3v2Frame> getAll(String type) {
		List<ID3v2Frame> list = framesByID.get(type);
		if (list==null || list.isEmpty()) return new ArrayList<ID3v2Frame>();
		return new ArrayList<ID3v2Frame>(framesByID.get(type));
	}

	//-----------------------------------------------------------------
	public List<ID3v2Frame> getAll(ID3FrameType type) {
		return getAll(type.name());
	}

	//-----------------------------------------------------------------
	public String getTXXHeader(String key) {
		for (ID3v2Frame frame : getAll(ID3FrameType.TXXX)) {
			if ((frame instanceof UserDefinedTextFrame) && ((UserDefinedTextFrame)frame).getDescription().equals(key)) {
				return ((UserDefinedTextFrame)frame).getValue();
			}
		}
		return null;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the flags
	 */
	public BitSet getFlags() {
		return flags;
	}

	//-----------------------------------------------------------------
	/**
	 * @param flags the flags to set
	 */
	public void setFlags(BitSet flags) {
		this.flags = flags;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the tagSize
	 */
	public int getTagSize() {
		return tagSize;
	}

	//-----------------------------------------------------------------
	/**
	 * @param tagSize the tagSize to set
	 */
	public void setTagSize(int tagSize) {
		this.tagSize = tagSize;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the parser
	 */
	public TagParser getParser() {
		return parser;
	}

	//-----------------------------------------------------------------
	/**
	 * @param parser the parser to set
	 */
	public void setParser(TagParser parser) {
		this.parser = parser;
	}

}
