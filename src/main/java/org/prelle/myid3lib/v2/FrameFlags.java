/**
 * 
 */
package org.prelle.myid3lib.v2;

/**
 * @author prelle
 *
 */
public enum FrameFlags {

	TAG_ALTER_PRESERVATION(7),
	FILE_ALTER_PRESERVATION(6),
	READ_ONLY(5),
	COMPRESSION(15),
	ENCRYPTION(14),
	GROUPING_IDENTITY(13),
	;
	
	private int bit;
	private FrameFlags(int bit) { 
		this.bit = bit;
	}
	public int getBitInBitSet() {
		return bit;
	}
	public static FrameFlags getForBitFromBitSet(int bit) {
		for (FrameFlags flag : FrameFlags.values())
			if (flag.getBitInBitSet()==bit)
				return flag;
		throw new IllegalArgumentException();
	}
}
