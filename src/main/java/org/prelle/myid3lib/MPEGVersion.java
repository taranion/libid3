/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public enum MPEGVersion {

	VERSION_2_5,
	RESERVED,
	VERSION_2,
	VERSION_1,
	
}
