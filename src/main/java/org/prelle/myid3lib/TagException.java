/**
 * 
 */
package org.prelle.myid3lib;

/**
 * Thrown whenever the tag value is wrong or unexpected.
 * 
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class TagException extends Exception {

	//-----------------------------------------------------------------
	/**
	 * @param message
	 */
	public TagException(String message) {
		super(message);
	}

}
