/**
 * 
 */
package org.prelle.myid3lib;

/**
 * @author prelle
 *
 */
public enum InfoType {

	TITLE, // Song title
	ARTIST, // Lead performer
	ALBUM, // Album title
	TIME_RELEASED,
	COMMENT,
	GENRE,
	TRACK,
	TRACKS_TOTAL,
	
}
