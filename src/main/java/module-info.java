module libid3 {
	exports org.prelle.myid3lib.frame;
	exports org.prelle.myid3lib.v2;
	exports org.prelle.myid3lib.v1;
	exports org.prelle.myid3lib;

	requires org.apache.logging.log4j;
}