/**
 *
 */
package test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.prelle.myid3lib.TagReader;
import org.prelle.myid3lib.TaggedFile;

/**
 * @author prelle
 *
 */
public class ReadV10Tag {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {
		Path file = FileSystems.getDefault().getPath("/home/mp3/rpgcd1/060_Diablo2_-_Jungle.mp3");
		TaggedFile tagged = TagReader.read(file);
		System.out.println("Read "+tagged);
		System.out.println("V1 = "+tagged.getV1());
	}

}
