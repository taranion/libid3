/**
 *
 */
package test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.prelle.myid3lib.TagReader;
import org.prelle.myid3lib.TaggedFile;

/**
 * @author prelle
 *
 */
public class ReadV23Tag {

	private static String[] FILES = {
		"/home/mp3/rollenspiel/irish/Bailey's Dance Music- Traditional Irish Fiddle Music.mp3",
		"/home/mp3/rpgcd2/Albernia/Atlan_-_The_Wedding_Jig.mp3",
		"/home/mp3/Bahamuth/Kampf/Kampf_allgemein1.mp3",
		"/home/mp3/Bahamuth/Kampf/Kampf_allgemein2.mp3",
		"/home/mp3/Abenteuer/GrenzMacht/02Ron_Reise.mp3",
		"/home/mp3/Abenteuer/GrenzMacht/07Ron_Brandanschlag.mp3",
		"/home/mp3/Computerspiele/Wizardry8_-_T'Rrang.mp3",
		"/home/mp3/Computerspiele/UltimaRemakes/Caverns.mp3",
		"/home/mp3/Computerspiele/Miller,_Robyn_-_Myst__The_Soundtrack/05_The_Tower.mp3",
		"/home/mp3/Computerspiele/Risen_2/Native_Village_(Day_Explore).mp3",
		"/home/mp3/Soundtrack/Lord of the Rings - 11 - The Ring Goes South.mp3",
		"/home/mp3/Soundtrack/Star Wars - Part II/04 - The Walls Converge.mp3",
		"/home/mp3/Soundtrack/Contact_-_Movie_Theme.mp3",
		"/home/mp3/Soundtrack/2005 Casanova/23 - The Hanging.mp3",
		"/home/mp3/rpgcd2/Spannung_und_Action/Batman_Begins_-_Molossus.mp3",
		"/home/mp3/Bahamuth/Hoehlen/Grotte.mp3",
	};

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {
//		Path file = FileSystems.getDefault().getPath("/home/mp3/rollenspiel/irish/Bailey's Dance Music- Traditional Irish Fiddle Music.mp3");
//		Path file = FileSystems.getDefault().getPath("/home/mp3/rpgcd2/Albernia/Atlan_-_The_Wedding_Jig.mp3");
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Bahamuth/Kampf/Kampf_allgemein1.mp3");
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Bahamuth/Kampf/Kampf_allgemein2.mp3"); // V2.4
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Abenteuer/GrenzMacht/02Ron_Reise.mp3");
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Abenteuer/GrenzMacht/07Ron_Brandanschlag.mp3");
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Computerspiele/Wizardry8_-_T'Rrang.mp3"); // V2.2
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Computerspiele/UltimaRemakes/Caverns.mp3"); // Flags
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Computerspiele/Miller,_Robyn_-_Myst__The_Soundtrack/05_The_Tower.mp3"); // Flags
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Computerspiele/Risen_2/Native_Village_(Day_Explore).mp3"); //
//		Path file = FileSystems.getDefault().getPath("/home/mp3/Soundtrack/Lord of the Rings - 11 - The Ring Goes South.mp3"); //

//		ID3v2_3 old = new ID3v2_3(new RandomAccessFile(file.toFile(), "r"));
//		System.out.println("old "+old);

		for (String name : FILES) {
			Path file = FileSystems.getDefault().getPath(name);
			System.out.println("Read "+file);
			TaggedFile tagged = TagReader.read(file);
			System.out.println("V1 = "+tagged.getV1());
			System.out.println("V2 = "+tagged.getV2());
		}
	}

}
